//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !\defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook14 extends _HOOK_CLASS_
{
	public function hasIntegration(string $name): bool
	{
		$integration = \IPS\Db::i()->select( 'skript_integrations', 'core_members', array( 'name=?', parent::get_name() ) )->first();
		$integration = explode(",", $integration);
    	return in_array($name, $integration);
	}
}
