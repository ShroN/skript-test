//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !\defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook16 extends _HOOK_CLASS_
{
    public function POSTintegration($id)
    {
        if (!isset(\IPS\Request::i()->service))
            throw new \IPS\Api\Exception('NO_SERVICE', '', 404);

        if (!isset(\IPS\Request::i()->add))
            throw new \IPS\Api\Exception('NO_ADD', '', 404);

        $service = \IPS\Request::i()->service;
        $add = \IPS\Request::i()->add;
        $member = \IPS\Member::load($id);
        $has = $member->hasIntegration($service);

        $integration = \IPS\Db::i()->select('skript_integrations', 'core_members', array('name=?', $member->member_id))->first();
        $integration = explode(",", $integration);

        if ($add) {
            if (!$has) {
                $integration[] = $service;
            }
        } else {
            if ($has) {
                $key = array_search($service, $integration);
                if ($key)
                    unset($integration[$key]);
            }
        }
        $integration = implode(",", $integration);
        \IPS\Db::i()->update("core_members", array("skript_integrations" => $integration), array("member_id", $member->member_id));
    }
}
